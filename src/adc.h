/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * adc.h
 * pH measurement
 *
 */

#pragma once

#include <stm32f0xx.h>


#define ADC_CH_LOAD    (ADC_CHSELR_CHSEL5)
#define ADC_CH_BATTERY (ADC_CHSELR_CHSEL7)


static inline void ADC_Init()
{
  /* Enable clock on ADC1 */
  RCC->APB2ENR |= RCC_APB2ENR_ADCEN;

  /* clock: PCLK/2 (4MHz) */
  ADC1->CFGR2 = ADC_CFGR2_CKMODE_0;

  /* Calibrate ADC1 */
  ADC1->CR = ADC_CR_ADCAL;
  while(ADC1->CR & ADC_CR_ADCAL)
    ;
  /* Enable ADC1 */
  ADC1->CR = ADC_CR_ADEN;
  while(!(ADC1->ISR & ADC_ISR_ADRDY))
    ;

  /* 12-bit, continuous mode */
  ADC1->CFGR1 = ADC_CFGR1_CONT;
  /* sampling time: 55.5 1/f_ADC; ~60kSps */
  ADC1->SMPR = ADC_SMPR_SMP_2|ADC_SMPR_SMP_0;
}

uint16_t ADC_Measure(uint16_t channel, uint8_t avg);
