/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * lcd.h
 * HD44780 LCD driver
 *
 */

#pragma once

#include <stm32f0xx.h>

#include "gpio.h"
#include "delay.h"

//----------------------------------------------------------------------
// Source: http://radzio.dxp.pl/hd44780/
#define HD44780_CLEAR					0x01
#define HD44780_HOME					0x02

#define HD44780_ENTRY_MODE				0x04
	#define HD44780_EM_SHIFT_CURSOR		0
	#define HD44780_EM_SHIFT_DISPLAY	1
	#define HD44780_EM_DECREMENT		0
	#define HD44780_EM_INCREMENT		2

#define HD44780_DISPLAY_ONOFF			0x08
	#define HD44780_DISPLAY_OFF			0
	#define HD44780_DISPLAY_ON			4
	#define HD44780_CURSOR_OFF			0
	#define HD44780_CURSOR_ON			2
	#define HD44780_CURSOR_NOBLINK		0
	#define HD44780_CURSOR_BLINK		1

#define HD44780_DISPLAY_CURSOR_SHIFT	0x10
	#define HD44780_SHIFT_CURSOR		0
	#define HD44780_SHIFT_DISPLAY		8
	#define HD44780_SHIFT_LEFT			0
	#define HD44780_SHIFT_RIGHT			4

#define HD44780_FUNCTION_SET			0x20
	#define HD44780_FONT5x7				0
	#define HD44780_FONT5x10			4
	#define HD44780_ONE_LINE			0
	#define HD44780_TWO_LINE			8
	#define HD44780_4_BIT				0
	#define HD44780_8_BIT				16

#define HD44780_CGRAM_SET				0x40
#define HD44780_DDRAM_SET				0x80
//----------------------------------------------------------------------


void _LCD_Write(uint8_t data);


static inline void _LCD_WriteCmd(uint8_t data)
{
  LCD_RS_UNSET();
  _LCD_Write(data);
}


static inline void _LCD_WriteData(uint8_t data)
{
  LCD_RS_SET();
  _LCD_Write(data);
}


static inline void LCD_Init()
{
  for(uint8_t i = 0; i < 3; i++)
  {
    LCD_E_SET();
    LCD_DATA(0x3);
    Delay_us(1);
    LCD_E_UNSET();
    Delay_ms(5);
  }

  LCD_E_SET();
  LCD_DATA(0x2);  // 4 bit mode
  Delay_us(1);
  LCD_E_UNSET();
  Delay_us(50);

  _LCD_WriteCmd(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT);
  _LCD_WriteCmd(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF);
  _LCD_WriteCmd(HD44780_CLEAR);
  Delay_ms(2);
  _LCD_WriteCmd(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);
  _LCD_WriteCmd(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK);
}

void LCD_Clear(void);
void LCD_GoTo(uint8_t x, uint8_t y);
void LCD_WriteStr(char *str);
void LCD_WriteNum(uint16_t num, uint8_t size);
