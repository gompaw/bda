/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * delay.h
 * Delay functions
 *
 */

#include <stm32f0xx.h>


/* Delay based on SysTick counter
 * us - time in microseconds
 */
void Delay_us(uint32_t us)
{
  SysTick->VAL = 0;
  
  while(us--){
    while(!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk))
      ;
  }
}


void Delay_ms(uint16_t ms)
{
  while(ms--){
    Delay_us(1000);
  }
}
