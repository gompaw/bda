/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * delay.h
 * Delay functions
 *
 */

#pragma once

#include <stm32f0xx.h>
#include <core_cm0.h>


/* SysTick_Config code from core_cm0.h, but without interrupt */
static inline void My_SysTick_Config(uint32_t ticks)
{
  if ((ticks - 1UL) > SysTick_LOAD_RELOAD_Msk) { return; }          /* Reload value impossible */

  SysTick->LOAD  = (uint32_t)(ticks - 1UL);                         /* set reload register */
  SysTick->VAL   = 0UL;                                             /* Load the SysTick Counter Value */
  SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk |
                   SysTick_CTRL_ENABLE_Msk;                         /* Enable SysTick Timer */
}

static inline void Delay_Init(void)
{
  /* Set SysTick for 1us interval */
  My_SysTick_Config(SystemCoreClock/1000000);
}

/* Delay based on SysTick counter
 * us - time in microseconds
 */
void Delay_us(uint32_t us);

/* Delay milliseconds
 */
void Delay_ms(uint16_t ms);
