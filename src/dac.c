#include <stm32f0xx.h>

#include "dac.h"

void DAC_SetValue(uint16_t value)
{
  DAC->DHR12R1 = value;
}
