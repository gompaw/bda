/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * gpio.h
 *
 */

#pragma once

#include <stm32f0xx.h>

#define LOAD_01A 8
#define LOAD_05A 9
#define LOAD_20A 10
#define LOAD_ON(num) (GPIOA->ODR = (GPIOA->ODR&~(0x7<<8))|(1<<(num)))
#define LOAD_OFF() (GPIOA->ODR = (GPIOA->ODR&~(0x7<<8)))

#define BUZZER_ON() (GPIOA->BSRR = GPIO_BSRR_BR_12)
#define BUZZER_OFF() (GPIOA->BSRR = GPIO_BSRR_BS_12)
#define LED_Y_ON() (GPIOB->BSRR = GPIO_BSRR_BS_2)
#define LED_Y_OFF() (GPIOB->BSRR = GPIO_BSRR_BR_2)
#define LED_G_ON() (GPIOB->BSRR = GPIO_BSRR_BS_1)
#define LED_G_OFF() (GPIOB->BSRR = GPIO_BSRR_BR_1)
#define LED_R_ON() (GPIOB->BSRR = GPIO_BSRR_BS_0)
#define LED_R_OFF() (GPIOB->BSRR = GPIO_BSRR_BR_0)

#define LCD_ON() (GPIOA->BSRR = GPIO_BSRR_BR_6)
#define LCD_OFF() (GPIOA->BSRR = GPIO_BSRR_BS_6)
#define LCD_E_SET() (GPIOB->BSRR = GPIO_BSRR_BS_8)
#define LCD_E_UNSET() (GPIOB->BSRR = GPIO_BSRR_BR_8)
#define LCD_RS_SET() (GPIOB->BSRR = GPIO_BSRR_BS_3)
#define LCD_RS_UNSET() (GPIOB->BSRR = GPIO_BSRR_BR_3)
#define LCD_DATA(data) (GPIOB->ODR = (GPIOB->ODR&~(0xF<<4))|((data)<<4))

#define SW1_STATE (!(GPIOA->IDR&GPIO_IDR_1))
#define SW2_STATE (!(GPIOA->IDR&GPIO_IDR_2))
#define SW3_STATE (!(GPIOA->IDR&GPIO_IDR_0))
#define SW4_STATE (!(GPIOA->IDR&GPIO_IDR_3))


static inline void GPIO_Init(void)
{
  /* Enable clock on ports A, B */
  RCC->AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;

  /* GPIOA:
   *  0(IN/PU)  :   SW_3
   *  1(IN/PU)  :   SW_4
   *  2(IN/PU)  :   SW_2
   *  3(IN/PU)  :   SW_1
   *  4(DAC_1)  :   LOAD_CURRENT
   *  5(ADC_5)  :   LOAD_VOLTAGE
   *  6(OUT)    :   LCD_POWER
   *  7(ADC_7)  :   BATTERY_VOLTAGE
   *  8(OUT)    :   LOAD_01A
   *  9(OUT)    :   LOAD_05A
   *  10(OUT)   :   LOAD_20A
   *  11(AF2)   :   LCD_CONV / TIM1_CH4
   *  12(OUT)   :   BUZZER
   *  13(AF0/PU):   SWDIO
   *  14(AF1)   :   USART2_TX / SWCLK
   *  15(AF1)   :   USART2_RX
   */
  GPIOA->MODER = GPIO_MODER_MODER4_0|GPIO_MODER_MODER4_1|
                 GPIO_MODER_MODER5_0|GPIO_MODER_MODER5_1|
                 GPIO_MODER_MODER6_0|
                 GPIO_MODER_MODER7_0|GPIO_MODER_MODER7_1|
                 GPIO_MODER_MODER8_0|
                 GPIO_MODER_MODER9_0|
                 GPIO_MODER_MODER10_0|
                 GPIO_MODER_MODER11_1|
                 GPIO_MODER_MODER12_0|
                 GPIO_MODER_MODER13_1|
                 GPIO_MODER_MODER14_1|
                 GPIO_MODER_MODER15_1;

  GPIOA->PUPDR = GPIO_PUPDR_PUPDR0_0|
                 GPIO_PUPDR_PUPDR1_0|
                 GPIO_PUPDR_PUPDR2_0|
                 GPIO_PUPDR_PUPDR3_0|
                 GPIO_PUPDR_PUPDR4_0|
                 GPIO_PUPDR_PUPDR13_0|
                 GPIO_PUPDR_PUPDR14_1;

  GPIOA->AFR[1] = (0x2<<(3*4))|
                  (0x1<<(6*4))|
                  (0x1<<(7*4));

  /* GPIOB:
   *  0(OUT)    :    LED_Y
   *  1(OUT)    :    LED_G
   *  2(OUT)    :    LED_R
   *  3(OUT)    :    LCD_RS
   *  4(OUT)    :    LCD_DB4
   *  5(OUT)    :    LCD_DB5
   *  6(OUT)    :    LCD_DB6
   *  7(OUT)    :    LCD_DB7
   *  8(OUT)    :    LCD_E
   */
  GPIOB->MODER = GPIO_MODER_MODER0_0|
                 GPIO_MODER_MODER1_0|
                 GPIO_MODER_MODER2_0|
                 GPIO_MODER_MODER3_0|
                 GPIO_MODER_MODER4_0|
                 GPIO_MODER_MODER5_0|
                 GPIO_MODER_MODER6_0|
                 GPIO_MODER_MODER7_0|
                 GPIO_MODER_MODER8_0;
}
