/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * main.c
 *
 */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stm32f0xx.h>
#include <core_cm0.h>

#include "gpio.h"
#include "delay.h"
#include "adc.h"
#include "lcd.h"
#include "dac.h"
#include "lampa_ładunkowa.h"

uint16_t dac_val = 0;

int main(void)
{
  GPIO_Init();
  Delay_Init();
  DAC_Init();
  ADC_Init();

  LCD_Init();
  LCD_Clear();
  LCD_WriteStr("POG & WK");
  LCD_GoTo(0, 1);
  LCD_WriteStr("==BDAN==");

  lampa_3adunkowa();

  LOAD_ON(LOAD_20A);
  LCD_Clear();
  for(;;){
    if(SW1_STATE) {
      LCD_GoTo(0, 0);
      LCD_WriteStr("SW1");
    }

    if(SW2_STATE) {
      LCD_GoTo(0, 0);
      LCD_WriteStr("SW2");
    }

    if(SW3_STATE) {
      LCD_GoTo(0, 0);
      LCD_WriteStr("SW3");
    }

    if(SW4_STATE) {
      LCD_GoTo(0, 0);
      LCD_WriteStr("SW4");
    }

    DAC_SetValue(4095);

    LCD_GoTo(2, 1);
    LCD_WriteNum(((uint32_t)ADC_Measure(ADC_CH_LOAD, 9)*52672)>>16, 4);
    LCD_WriteStr("mV");

    Delay_ms(100);

  }
}

