/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * lcd.c
 * HD44780 LCD driver
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stm32f0xx.h>

#include "lcd.h"


void _LCD_Write(uint8_t data)
{
  LCD_E_SET();
  LCD_DATA(data>>4);
  Delay_us(1);
  LCD_E_UNSET();
  Delay_us(1);

  LCD_E_SET();
  LCD_DATA(data&0xF);
  Delay_us(1);
  LCD_E_UNSET();
  Delay_us(50);
}


void LCD_Clear(void)
{
  _LCD_WriteCmd(HD44780_CLEAR);
  Delay_ms(2);
  //~_LCD_WriteCmd(HD44780_HOME);
  //~Delay_ms(2);
}


void LCD_GoTo(uint8_t x, uint8_t y)
{
  _LCD_WriteCmd(HD44780_DDRAM_SET | (x + (0x40 * y)));
}


void LCD_WriteStr(char *str)
{
  register char *ptr = str;

  while(*ptr){
    _LCD_WriteData(*ptr++);
  }
}

/*
void LCD_WriteNum(uint16_t num, uint8_t size)
{
  char buf[6];
  register uint8_t len;

  itoa(num, buf, 10);
  len = strlen(buf);
  for(uint8_t rem=size-len; rem>0; rem--)
    _LCD_WriteData(' ');

  for(uint8_t i=0; i<len; i++)
    _LCD_WriteData(buf[i]);

}
*/


void LCD_WriteNum(uint16_t num, uint8_t size)
{
  char buf[6];
  register uint8_t len;

  itoa(num, buf, 10);
  len = strlen(buf);
  size -= len;
  while(size--){
    _LCD_WriteData(' ');
  }
  LCD_WriteStr(buf);
}


