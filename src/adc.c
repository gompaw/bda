/*
 * Copyright (C) 2016 Wojciech Krutnik <wojciechk8@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * adc.c
 *
 */

#include <stm32f0xx.h>

#include "adc.h"


uint16_t ADC_Measure(uint16_t channel, uint8_t avg)
{
  uint32_t result = 0;
  uint16_t avg_cnt = 1<<avg;

  ADC1->CHSELR = channel;

  ADC1->CR = ADC_CR_ADSTART;
  while(avg_cnt--){
    while(!(ADC1->ISR & ADC_ISR_EOSEQ))
      ;
    /* Acknowledge the conversion */
    ADC1->ISR = ADC_ISR_EOSEQ;

    result += ADC1->DR;
  }

  ADC1->CR = ADC_CR_ADSTP;
  while(ADC1->CR & ADC_CR_ADSTP)
    ;

  return result >>= avg;
}

