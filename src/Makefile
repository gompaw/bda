# The name for the project
TARGET:=bda

# The CPU architecture (will be used for -mcpu)
MCPU:=cortex-m0
MCU:=STM32F051x8

# The source files of the project
SRC:=$(wildcard *.c)

# Include directory for the system include files and system source file
SYSDIR:=sys
SYSSRC:=sys/system_stm32f0xx.c

# Other include directories
INCDIR:=u8glib

# The name of the startup file which matches to the architecture (MCPU)
STARTUP:=sys/startup_ARMCM0.S
STARTUP_DEFS=-D__STARTUP_CLEAR_BSS -D__START=main

# Include directory for CMSIS
CMSISDIR:=/opt/STM32Cube_FW_F0_V1.4.0/Drivers/CMSIS

# Name of the linker script
LDSCRIPT:=sys/gcc.ld

# Target objects directory
OBJDIR:=obj



# Internal Variable Names
ELF:=$(OBJDIR)/$(TARGET).elf
HEX:=$(OBJDIR)/$(TARGET).hex
DIS:=$(OBJDIR)/$(TARGET).dis
MAP:=$(OBJDIR)/$(TARGET).map
OBJ:=$(SRC:%.c= $(OBJDIR)/%.o) $(SYSSRC:$(SYSDIR)/%.c= $(OBJDIR)/%.o) $(STARTUP:$(SYSDIR)/%.S= $(OBJDIR)/%.o)

# Replace standard build tools by arm tools
CC:=arm-none-eabi-gcc
AS:=arm-none-eabi-gcc
OBJCOPY:=arm-none-eabi-objcopy
OBJDUMP:=arm-none-eabi-objdump
SIZE:=arm-none-eabi-size

# Common flags
COMMON_FLAGS = -mthumb -mlittle-endian -mcpu=$(MCPU)
COMMON_FLAGS += -Wall
COMMON_FLAGS += -I. -I$(CMSISDIR)/Include -I$(CMSISDIR)/Device/ST/STM32F0xx/Include -I$(SYSDIR) -I$(INCDIR)
COMMON_FLAGS += -D$(MCU)
COMMON_FLAGS += -Os -flto -g -ffunction-sections -fdata-sections
# Assembler flags
ASFLAGS:=$(COMMON_FLAGS)
# C flags
CFLAGS:=$(COMMON_FLAGS) -std=gnu11
# LD flags
LDFLAGS:=$(COMMON_FLAGS) -Wl,--gc-sections -Wl,-Map=$(MAP) -Wl,--no-wchar-size-warning
LDLIBS:=--specs=nano.specs --specs=nosys.specs -T$(LDSCRIPT)

# Dependecies
DEPENDS:=$(SRC:%.c=$(OBJDIR)/%.d)


# Additional Suffixes
.SUFFIXES: .elf .hex .dis

# Targets
.PHONY: all
all: $(DIS) $(HEX)
	$(SIZE) $(ELF)

.PHONY: program
program: $(DIS) $(HEX) $(ELF)
	openocd -f openocd.cfg \
					-c "program $(ELF) verify reset exit"
	$(SIZE) $(ELF)

.PHONY: run
run: $(DIS) $(HEX) $(ELF)
	openocd -f openocd.cfg \
					-c "init" -c "reset" -c "exit"

.PHONY: clean
clean:
	$(RM) $(OBJ) $(HEX) $(ELF) $(DIS) $(MAP)

# implicit rules
.elf.hex:
	$(OBJCOPY) -O ihex $< $@

$(OBJDIR)/%.o: %.c
	$(CC) -MMD $(CFLAGS) -c $< -o $@

$(OBJDIR)/%.o: $(SYSDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJDIR)/%.o: $(SYSDIR)/%.S
	$(CC) $(CFLAGS) -c $< -o $@

# explicit rules
$(ELF): $(OBJ)
	$(LINK.o) $(LDFLAGS) $(LDLIBS) $^ -o $@

$(DIS): $(ELF)
	$(OBJDUMP) -S $< > $@

# include dependecies
-include $(DEPENDS)
