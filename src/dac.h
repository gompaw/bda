#pragma once

static inline void DAC_Init()
{
  RCC->APB1ENR |= RCC_APB1ENR_DACEN;
  DAC->CR |= DAC_CR_EN1;
}

void DAC_SetValue(uint16_t value);
