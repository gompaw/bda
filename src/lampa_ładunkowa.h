#pragma once

static inline void lampa_3adunkowa(void)
{
  register uint16_t x = SystemCoreClock/100000;

  RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;

  /* Toggle output on compare match */
  TIM1->CCMR2 = TIM_CCMR2_OC4M_0|TIM_CCMR2_OC4M_1;
  TIM1->CCER = TIM_CCER_CC4E;
  TIM1->BDTR = TIM_BDTR_MOE;
  /* CK_CNT = 2kHz, f_OUT = 1kHz */
  TIM1->PSC = x;
  TIM1->ARR = 1;
  TIM1->CCR4 = 1;

  TIM1->CR1 |= TIM_CR1_CEN;

  for(; x < SystemCoreClock/1300; x++){
    TIM1->PSC = x;
    Delay_us(250);
  }
}
